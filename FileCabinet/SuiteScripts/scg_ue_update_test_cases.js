/**
 *
 * @NApiVersion 2.0
 * @NScriptType usereventscript
 * @author Robert Fugate
 */
define(['N/record', 'N/search', 'N/runtime'], function (record, search, runtime) {

    function beforeSubmit(context) {
        if(context.type != context.UserEventType.CREATE){
            return;
        }

        var newObj = context.newRecord;
        var status = newObj.getValue({
            fieldId: 'custrecord_scg_testresults_status'
        });

        var testCaseID = newObj.getValue({
            fieldId: 'custrecord_scg_test_case_name'
        });
        log.debug('Status', status)
        var toCaseStatus = testStatusToCaseStatus(status)
        log.debug('Remapped Status', toCaseStatus)
        var testCasesRecord = record.load({
            type: 'customrecord_scg_test_cases',
            id: testCaseID,
            isDynamic: false
        })
        log.debug(testCasesRecord)
        testCasesRecord.setValue({
            fieldId: 'custrecord_scg_testcase_status',
            value: toCaseStatus
        })
        var id = testCasesRecord.save();
        log.debug(id)

        
    }

    function testStatusToCaseStatus(status){
        if(status == 1){
            return 5;
        }
        if(status == 2){
            return 4;
        }
        if(status == 3){
            return 3;
        }
        if(status == 4){
            return 6;
        }
    }


    function isNullorEmpty(checkVal){
        if(checkVal != null && checkVal != undefined && checkVal != ''){
            return false;
        }
        else{
            return true;
        }
    };

    return {
        beforeSubmit: beforeSubmit
    };
});
